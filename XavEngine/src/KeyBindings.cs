using Microsoft.Xna.Framework.Input;

namespace XavEngine
{
    public static class KeyBindings
    {
        public static Keys CameraMoveUp = Keys.W;
        public static Keys CameraMoveDown = Keys.S;
        public static Keys CameraMoveLeft = Keys.A;
        public static Keys CameraMoveRight = Keys.D;

    }
}