using Microsoft.Xna.Framework;

namespace XavEngine.Core.Components
{
    public struct LifeTime
    {
        public float StartTime {get; set;}
        public float Age {get; set;}
        public float MaxAge {get; set;}
    }

    public struct KillBounds
    {
        public Rectangle KillRect {get; set;}
    }
}