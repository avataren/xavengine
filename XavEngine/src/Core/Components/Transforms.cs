using DefaultEcs;
using Microsoft.Xna.Framework;

namespace XavEngine.Core.Components
{
    public struct TransformRoot
    {
    }

    public struct TransformChild
    {
        public Entity Parent {get; set;}
    }

    public struct Transform
    {
        public Matrix2D CreateMatrix(){
            var matT = Matrix2D.CreateTranslation(Position.X, Position.Y);
            var matS = Matrix2D.CreateScale(Scale);
            var matR = Matrix2D.CreateRotation(Rotation);
            var ret = matS * matR * matT;            
            return ret;
        }
        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public float Rotation { get; set; }

    }

}