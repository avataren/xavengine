using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XavEngine.Core.Components
{
    public struct DrawInfo
    {
        public bool InView {get; set;}
        public Vector2 Origin { get; set; }
        public Vector2 Center { get; set; }
        public Vector2 Size { get; set; }
        public Rectangle? SrcRect { get; set; }
        public Color Color { get; set; }
        public float LayerDepth {get; set;}
        public Matrix2D WorldTransform;
        public Texture2D Texture {get; set;}
    }

    public struct ScreenCoords
    {
        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public float Rotation { get; set; }
    }
}