using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XavEngine.Core.Components
{
    public struct Emitter
    {
        public float SpawnRate {get; set;}
        public float SpawnTracker {get; set;}
        public float Spread {get; set;}
        public Texture2D texture {get; set;}
    }

    public struct Particle
    {
        
    }

    public struct ColorOverTime {
        public Color StartColor {get; set;}
        public Color EndColor {get; set;}
    }

    public struct SizeOverTime{
        public Vector2 StartSize {get;set;}
        public Vector2 EndSizeSize {get;set;}

    }

    public struct SimplePhysics{
        public Vector2 Velocity {get;set;}
        public Vector2 Acceleration {get;set;}

        public float AngularVelocity {get;set;}
    }


}