using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XavEngine.Core.Loaders;

namespace XavEngine.Core.Components
{
    public class AnimationSequence
    {
        public string Name {get; set;}
        public int FPS {get; set;}
        //List<int> FrameIndices { get; set; }
        public float FrameTimerAccum {get; set;}
        public IList<SpriteSheetFrame> Frames { get; set; }
        private int FrameIndex { get; set; }
        public AnimationSequence(int fps=10)
        {
            FPS = fps;
            Frames = new List<SpriteSheetFrame>();
            FrameIndex = 0;
        }

        public void Reset(){
            FrameTimerAccum=0f;
            FrameIndex = 0;
        }
        public void AddFrameToSequence(SpriteSheetFrame frame)
        {
            Frames.Add(frame);
        }

        public SpriteSheetFrame GetFrame()
        {
            return Frames[FrameIndex];
        }

        public void Update(float deltaTime)
        {
            var invFPS = 1f / FPS;
            FrameTimerAccum += deltaTime;
            if (FrameTimerAccum > invFPS)
            {
                FrameTimerAccum = 0;
                NextFrame();
            }
        }

        public void NextFrame()
        {
            if (FrameIndex == Frames.Count - 1)
            {
                FrameIndex=0;
            }
            else
                FrameIndex++;
        }

        public void RandomizeFrame(){
            var rnd = new System.Random();
            FrameIndex= rnd.Next(0,Frames.Count);
        }
    }

    public class Animation
    {
        public SpriteSheet sheet;
        public List<AnimationSequence> Sequences {get; set;}
        public AnimationSequence CurrentSequence {get; set;}
        
        public Animation(SpriteSheet sheet)
        {
            this.sheet = sheet;
            Sequences = new List<AnimationSequence>
            {
                new AnimationSequence{
                    Name ="Default",
                    FPS = 11,
                }
            };

            sheet.Frames.ForEach(f=>{
                Sequences[0].AddFrameToSequence(f);
            });

            CurrentSequence = Sequences[0];

        }
        public void RandomizeFrame(){
            CurrentSequence.RandomizeFrame();
        }
        public void Reset()
        {
            CurrentSequence.Reset();
        }

        public void Update(float deltaTime)
        {
            CurrentSequence.Update(deltaTime);
        }

        public Rectangle GetSrcRect()
        {
            var rect =  CurrentSequence.GetFrame().Frame;
            return rect;
        }

        public Texture2D GetTexture()
        {
            return sheet.Texture;
        }

        public Vector2 GetCenter()
        {
            return CurrentSequence.GetFrame().SourceSize * 0.5f;
        }

        public Vector2 GetSize()
        {
            return CurrentSequence.GetFrame().SourceSize;
        }

        public Vector2 GetOrigin()
        {
            return GetCenter();//
            //return CurrentSequence.GetFrame().SourceSize * CurrentSequence.GetFrame().Pivot;
        }
    }
}