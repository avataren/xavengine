using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.IO;
using SpriteFontPlus;

namespace XavEngine.Core
{
    public class ConsoleEntry
    {
        public DateTime time;
        public string entry;
        public long id;
    }

    public class Console
    {
        float timeStamp = 0f;
        long logCount = 0;
        private SpriteFont font;
        private readonly SpriteBatch spriteBatch = null;
        readonly LinkedList<ConsoleEntry> entries = new LinkedList<ConsoleEntry>();
        List<ConsoleEntry> logsToDisplay = new List<ConsoleEntry>();
        readonly int maxLogLength = 1000;
        readonly Color colorFont = Color.White;
        readonly Color colorBackgrond = Color.FromNonPremultiplied(0, 0, 0, 192);
        Rectangle dstRect = new Rectangle();
        Rectangle bounds = new Rectangle();
        Texture2D backgroundTex;
        readonly GraphicsDevice device;
        public bool Visible { get; set; }
        readonly int bottomMargin = 5;
        readonly int topMargin = 5;
        readonly int pageMargin = 5;
        readonly int inputTopMarginLines = 1;
        float lastBlink = 0;
        readonly float blinkRate = 0.5f;
        bool cursorBlink = true;
        KeyboardState previousState = Keyboard.GetState();
        bool showFPS = true;
        long scroll = 0;
        string currentInput = "";
        ulong FrameCount { get; set; }

        public static Console Instance { get; set; }
        public Console(GraphicsDevice device)
        {
            this.device = device;
            spriteBatch = new SpriteBatch(device);
            Visible = false;
            backgroundTex = new Texture2D(device, 1, 1);
            backgroundTex.SetData(new[] { Color.White });
            FrameCount = 1;
            Instance = this;
        }

        public void LogEntry(string entry)
        {
            entries.AddLast(new ConsoleEntry { time = DateTime.Now, entry = entry, id = logCount++ });
            while (entries.Count > maxLogLength)
                entries.RemoveFirst();

            UpdateLogsToDisplay();
        }

        private void UpdateLogsToDisplay()
        {
            logsToDisplay = entries.Reverse().Where(e => e.id < (logCount - scroll)).Take(maxLines).ToList();
        }

        public static void Log(string entry)
        {
            Console.Instance.LogEntry(entry);
        }

        public static byte[] ExtractResource(String filename)
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            using (Stream resFilestream = a.GetManifestResourceStream(filename))
            {
                if (resFilestream == null) return null;
                byte[] ba = new byte[resFilestream.Length];
                resFilestream.Read(ba, 0, ba.Length);
                return ba;
            }
        }

        public void LoadContent()
        {

            var fontFile = "Content/gfx/fonts/Hack-Regular.ttf";
            using (var stream = TitleContainer.OpenStream(fontFile))
            {
                var fontBakeResult = TtfFontBaker.Bake(stream,
                    25,
                    1024,
                    1024,
                    new[]
                    {
                    CharacterRange.BasicLatin,
                    CharacterRange.Latin1Supplement,
                    CharacterRange.LatinExtendedA,
                    CharacterRange.Cyrillic
                    }
                );

                font = fontBakeResult.CreateSpriteFont(device);
            }
            // font = Content.Load<SpriteFont>("content/gfx/fonts/console");
        }

        void ProcessCommands()
        {
            if (string.IsNullOrEmpty(currentInput))
                return;

            if (currentInput.ToLower().StartsWith("fps"))
            {
                var split = currentInput.Split(' ');
                if (split.Length > 1 && split[1] == "0")
                    showFPS = false;
                else showFPS = true;
                currentInput = "";
                return;
            };
            /*
                        if (currentInput.ToLower().StartsWith("fullscreen"))
                        {
                            var split = currentInput.Split(' ');
                            if (split.Length > 1 && split[1] == "0")
                            {
                                //disable fs
                                Engine.Instance.Graphics.IsFullScreen = false;
                            }
                            else
                            {
                                //enter fs
                                Engine.Instance.Graphics.IsFullScreen = true;
                            }
                            Engine.Instance.Graphics.ApplyChanges();
                            currentInput = "";
                            return;
                        };*/

            if (currentInput.ToLower().StartsWith("listmodes"))
            {
                foreach (DisplayMode mode in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes)
                {
                    Log($"{mode}");
                }
                currentInput = "";
                return;
            };
            /*
                        if (currentInput.ToLower().StartsWith("mode"))
                        {
                            var split = currentInput.Split(' ');
                            if (split.Length >= 2)
                            {
                                int newWidth = Convert.ToInt32(split[1]);
                                int newHeight = Convert.ToInt32(split[2]);
                                if (newWidth > 0 && newHeight > 0)
                                {
                                    Engine.Instance.Graphics.PreferredBackBufferWidth = newWidth;
                                    Engine.Instance.Graphics.PreferredBackBufferHeight = newHeight;
                                    Engine.Instance.Graphics.ApplyChanges();
                                }
                            }
                            currentInput = "";
                            return;
                        };
            */
            if (currentInput.ToLower() == ("help") || currentInput.ToLower() == "?")
            {
                Log("----------------------------Commands---------------------------------");
                Log("tab                    : toggle console");
                Log("fps 0 / 1              : toggle fps counter");
                //Log("fullscreen 0 / 1       : toggle fullscreen");
                Log("listmodes              : list display modes");
                Log("mode width height      : set display modes");
                Log("quit                   : quit application");
                Log("---------------------------------------------------------------------");
                currentInput = "";
                return;
            };
            Log(currentInput);
            switch (currentInput.ToLower())
            {
                case "quit":
                    //Engine.Instance.Exit();
                    break;
            }
            currentInput = "";

        }
        public void Update(float deltaTime)
        {
            bounds = device.PresentationParameters.Bounds;
            HandleInput();
        }

        DateTime lastPress = DateTime.Now;
        private void HandleInput()
        {
            var delay = DateTime.Now - lastPress;
            var keys = Keyboard.GetState().GetPressedKeys();
            bool shift = IsShiftPressed(keys);

            if (delay.TotalSeconds > 0.05f)
            {
                lastPress = DateTime.Now;
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    scroll = Math.Max(0, scroll - (shift ? 8 : 1));
                    UpdateLogsToDisplay();
                }

                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                {
                    scroll = Math.Min(logCount, scroll + (shift ? 8 : 1));
                    UpdateLogsToDisplay();
                }
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Tab) && !previousState.IsKeyDown(Keys.Tab))
            {
                Visible = !Visible;
            }
            if (!Visible)
            {
                previousState = Keyboard.GetState();
                return;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !previousState.IsKeyDown(Keys.Enter))
            {
                ProcessCommands();
            }
            else
            {
                HandleTyping(keys, shift);
            }
            previousState = Keyboard.GetState();
        }

        private static bool IsShiftPressed(Keys[] keys)
        {
            bool shift = false;
            foreach (var key in keys)
            {
                if (key.Equals(Keys.LeftShift) || key.Equals(Keys.RightShift))
                {
                    shift = true;
                    break;
                }
            }
            return shift;
        }

        private void HandleTyping(Keys[] keys, bool shift)
        {
            foreach (var key in from key in keys
                                where !previousState.IsKeyDown(key)
                                select key)
            {
                if (key.Equals(Keys.Space))
                    currentInput += " ";
                else
                    if (key.Equals(Keys.Back))
                    currentInput = currentInput.Substring(0, Math.Max(0, currentInput.Length - 1));
                else if ((int)key > 64 && (int)key < 91)
                {
                    var keyValue = key.ToString();
                    if (!shift)
                        keyValue = keyValue.ToLower();
                    currentInput += keyValue;
                }
                else if ((int)key > 47 && (int)key < 58)
                {
                    var keyValue = key.ToString();
                    keyValue = keyValue.TrimStart('D');
                    if (shift)
                    {
                        switch ((int)key)
                        {
                            case 49:
                                keyValue = "!";
                                break;
                            case 50:
                                keyValue = "\"";
                                break;
                            case 51:
                                keyValue = "#";
                                break;
                            case 52:
                                keyValue = "$";
                                break;
                            case 53:
                                keyValue = "%";
                                break;
                            default:
                                break;
                        }
                    }
                    currentInput += keyValue;
                }
            }
        }

        public void Draw(ElapsedTime gameTime)
        {
            spriteBatch.Begin();

            if (Visible)
            {
                DrawConsole(gameTime);
            }
            if (showFPS)
            {
                DrawFPS(gameTime);
            }

            spriteBatch.End();
            ++FrameCount;
        }

        int maxLines = 1000;
        private void DrawConsole(ElapsedTime gameTime)
        {
            var consoleHeight = bounds.Height / 2;
            DrawConsoleBackground(consoleHeight);
            var scrHeight = consoleHeight;
            var lineHeight = font.LineSpacing;
            maxLines = Math.Max(0, scrHeight / lineHeight - inputTopMarginLines);
            int linesToDraw = Math.Min(maxLines, entries.Count);

            //var logs = entries.Reverse().Where(e=>e.id < (logCount-scroll)).Take(maxLines).ToList();

            int yPos = (int)(lineHeight * linesToDraw) + topMargin;
            foreach (var txt in logsToDisplay)
            {
                string line = $"{txt.time.ToLongTimeString()}: {txt.entry}";
                yPos -= (int)lineHeight;

                try
                {
                    spriteBatch.DrawString(font, line, new Vector2(pageMargin, yPos), colorFont);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }

            if (gameTime.TotalTime - lastBlink > blinkRate)
            {
                lastBlink = gameTime.TotalTime;
                cursorBlink = !cursorBlink;
            }

            var inputText = currentInput + (cursorBlink ? "_" : "");
            var inputTextHeight = font.MeasureString(inputText);
            spriteBatch.DrawString(font, inputText, new Vector2(pageMargin, consoleHeight + topMargin + bottomMargin - inputTextHeight.Y), colorFont);
        }

        private void DrawConsoleBackground(int consoleHeight)
        {
            dstRect.Width = bounds.Width;
            dstRect.Height = consoleHeight + topMargin + bottomMargin;
            spriteBatch.Draw(backgroundTex, dstRect, null, colorBackgrond);
        }

        ulong lastFrameCount = 1;
        double fps = 0;
        void DrawFPS(ElapsedTime gameTime)
        {
            if (font == null)
                return;

            var diff = gameTime.TotalTime - timeStamp;
            if (diff >= 0.5f)
            {
                ulong frames = Math.Max(1, FrameCount - lastFrameCount);
                fps = Mathf.RoundToInt(frames / diff);
                timeStamp = gameTime.TotalTime;
                lastFrameCount = FrameCount;
            }

            var line = $"{fps} FPS";
            var lineDim = font.MeasureString(line);
            spriteBatch.DrawString(font, line, new Vector2(bounds.Width / 2 - lineDim.X / 2 + 2, topMargin + 2), Color.Black);
            spriteBatch.DrawString(font, line, new Vector2(bounds.Width / 2 - lineDim.X / 2, topMargin), colorFont);
        }
    }
}