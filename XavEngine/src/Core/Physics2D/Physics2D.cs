using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.Xna.Framework;
using tainicom.Aether.Physics2D.Dynamics;

namespace XavEngine.Core.Physics2D
{
    public class Physics
    {
        public World World { get; set; }
        public float GravityConst = 9.81f;
        public bool Running { get; set; }
        public readonly object lockObj = new object();
        Stopwatch sw;
        readonly Thread thread = null;
        readonly int Frequency = 1000/144;
        public static float PixelsPrMeter = 100f;
        public static float PIXELS_TO_SI(float x) { return x/PixelsPrMeter; }
        public static int SI_TO_PIXELS(float x) {return Mathf.RoundToInt(x*PixelsPrMeter); }        
        public Physics()
        {
            World = new World
            {
                Gravity = new Vector2(0, GravityConst),
            };

            //World.ContactManager.VelocityConstraintsMultithreadThreshold = 256;
            //World.ContactManager.PositionConstraintsMultithreadThreshold = 256;
            //World.ContactManager.CollideMultithreadThreshold = 256;

            thread = new Thread(RunThread)
            {
                IsBackground = true
            };
            thread.Start();
        }

        public void RunThread()
        {
            Running = true;
            sw = Stopwatch.StartNew();
            long elapsed = sw.ElapsedMilliseconds;
            while (Running)
            {
                lock (lockObj)
                {
                    World.Step(TimeSpan.FromMilliseconds(Frequency));
                }
                long diff = sw.ElapsedMilliseconds - elapsed;
                long s = Frequency - diff;
                if (s < 1) s = 1;
                Thread.Sleep((int)s);
                elapsed = sw.ElapsedMilliseconds;
            }
        }
        public void Quit()
        {
            if (Running)
            {
                Running = false;
                thread.Join(1000);
            }
        }
    }
}