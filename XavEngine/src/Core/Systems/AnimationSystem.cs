using DefaultEcs;
using DefaultEcs.System;
using DefaultEcs.Threading;
using XavEngine.Core.Components;

namespace XavEngine.Core.Systems
{


    [With(typeof(Animation))]
    [With(typeof(DrawInfo))]
    public class AnimationSystem : AEntitySystem<float>
    {
        public AnimationSystem(World world, IParallelRunner runner)
            : base(world, runner)
        {
            
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref Animation anim = ref entity.Get<Animation>();
            ref DrawInfo dInfo = ref entity.Get<DrawInfo>();

            anim.Update(elaspedTime);

            dInfo.SrcRect = anim.GetSrcRect();
            dInfo.Texture = anim.GetTexture();
            dInfo.Origin = anim.GetOrigin();
            dInfo.Center = anim.GetCenter();
            dInfo.Size = anim.GetSize();
        }
    }
}