using System;
using DefaultEcs;
using DefaultEcs.System;
using DefaultEcs.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XavEngine.Core.Components;

namespace XavEngine.Core.Systems
{
    [With(typeof(BoundingBox))]
    [With(typeof(DrawInfo))]
    public sealed class CullingSystem : AEntitySystem<float>
    {
        readonly Camera camera;
        public CullingSystem(World world, Camera camera, IParallelRunner runner)
            : base(world, runner)
        {
            this.camera = camera;
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref BoundingBox boundingBox = ref entity.Get<BoundingBox>();
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            Vector2[] aabb_points = {   -drawInfo.Center,
                                        new Vector2(drawInfo.Center.X , -drawInfo.Center.Y),
                                        drawInfo.Center,
                                        new Vector2(-drawInfo.Center.X, drawInfo.Center.Y)
                                        };
            Vector2[] aabb_points_transformed = new Vector2[4];
            Vector2Ext.Transform(aabb_points, 0, ref drawInfo.WorldTransform, aabb_points_transformed, 0, aabb_points.Length);
            boundingBox.FromMinMax(aabb_points_transformed);
            drawInfo.InView = camera.AABB.CollisionCheck(boundingBox);
        }
    } 
}