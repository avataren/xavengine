using DefaultEcs;
using DefaultEcs.System;
using DefaultEcs.Threading;
using XavEngine.Core.Components;

namespace XavEngine.Core.Systems
{
    [With(typeof(SimplePhysics))]
    [With(typeof(Transform))]
    public class SimplePhysicsSystem : AEntitySystem<float>
    {
        public SimplePhysicsSystem(World world, IParallelRunner runner)
            : base(world, runner)
        {
        }
        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref Transform transform = ref entity.Get<Transform>();
            ref SimplePhysics simplePhysics = ref entity.Get<SimplePhysics>();

            transform.Position += simplePhysics.Velocity * elaspedTime;
            simplePhysics.Velocity += simplePhysics.Acceleration * elaspedTime;
            transform.Rotation += simplePhysics.AngularVelocity*elaspedTime;

        }
    }
}