using System.Collections.Generic;
using DefaultEcs;
using DefaultEcs.System;
using DefaultEcs.Threading;
using tainicom.Aether.Physics2D.Dynamics;
using XavEngine.Core.Components;

namespace XavEngine.Core.Systems
{
    [With(typeof(LifeTime))]
    public class LifeCycleSystem : AEntitySystem<float>
    {
        XavEngine.Core.Physics2D.Physics physics;
        List<Entity> oldEntities;
        public LifeCycleSystem(DefaultEcs.World world, XavEngine.Core.Physics2D.Physics physics)
            : base(world)
        {
            this.physics = physics;
            oldEntities = new List<Entity>();
            
        }

        protected override void PreUpdate(float state)
        {
            oldEntities.Clear();
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref LifeTime lifeTime = ref entity.Get<LifeTime>();
            lifeTime.Age += elaspedTime;

            if (lifeTime.Age > lifeTime.MaxAge)
            {
                if (entity.Has<Body>())  
                {
                    var b = entity.Get<Body>();
                    lock (physics.lockObj)
                    {
                        physics.World.Remove(b);
                    }
                    
                }
                oldEntities.Add(entity);
            }

        }

        protected override void PostUpdate(float state){
            if (oldEntities.Count > 0)
            {
                oldEntities.ForEach(e=>e.Dispose());
            }

        }
    }
}