using System;
using DefaultEcs;
using DefaultEcs.System;
using DefaultEcs.Threading;
using Microsoft.Xna.Framework;
using XavEngine.Core.Components;
using XavEngine.Core.Helpers;

namespace XavEngine.Core.Systems
{
    [With(typeof(Emitter))]
    public class EmitterSystem : AEntitySystem<float>
    {
        World _world;
        public EmitterSystem(World world)
            : base(world)
        {
            _world = world;
        }



        protected override void Update(float elapsedTime, in Entity entity)
        {
            ref Emitter emitter = ref entity.Get<Emitter>();

            float particlesPerFrame = emitter.SpawnRate * elapsedTime;
            emitter.SpawnTracker += particlesPerFrame;
            if (emitter.SpawnTracker >= 1)
            {
                int particlesToEmit = Mathf.FastFloorToInt(emitter.SpawnTracker);
                emitter.SpawnTracker -= particlesToEmit;

                for (int i=0;i<particlesToEmit;i++)
                {
                    SpawnParticle(ref emitter, entity);
                }
            }
        }

        protected void SpawnParticle(ref Emitter emitter, in Entity entity){
            var particle = SpriteHelper.CreateSprite(_world, emitter.texture);

            particle.Set(entity.Get<Transform>());
            System.Random rnd = new System.Random();
            var rndUnit = new Vector2((float)rnd.NextDouble()-0.5f,(float)rnd.NextDouble()-0.5f);
            rndUnit.Normalize();

            particle.Set(new SimplePhysics{
                Acceleration = new Vector2(0,500f),
                Velocity = rndUnit * 500f,
                AngularVelocity = rndUnit.X*10f
            });

            particle.Set(new LifeTime{
                MaxAge=1f
            });

            particle.Set(new ColorOverTime{
                StartColor = Color.FromNonPremultiplied(255,255,255,255),
                EndColor = Color.FromNonPremultiplied(255,128,0,255)
            });

            particle.Set(new Particle{

            });

            particle.Get<DrawInfo>().LayerDepth = 0.5f;
        }
    }



    [With(typeof(Particle))]
    [With(typeof(DrawInfo))]
    [With(typeof(LifeTime))]
    public class ParticleSystem : AEntitySystem<float>
    {
        public ParticleSystem(World world, IParallelRunner runner)
            : base(world, runner)
        {
        }

        protected override void Update(float elapsedTime, in Entity entity)
        {
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            ref LifeTime lifeTime = ref entity.Get<LifeTime>();
            float life = Math.Min(lifeTime.Age / lifeTime.MaxAge,1f);
            drawInfo.LayerDepth = life;
            if (entity.Has<ColorOverTime>())
            {
                ref ColorOverTime color = ref entity.Get<ColorOverTime>();
                drawInfo.Color = Color.Lerp(color.StartColor, color.EndColor, life);
            }                
        }
    }
    
}