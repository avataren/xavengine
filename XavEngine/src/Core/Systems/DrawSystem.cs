using DefaultEcs;
using DefaultEcs.System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XavEngine.Core.Components;

namespace XavEngine.Core.Systems
{
    [With(typeof(ScreenCoords))]
    [With(typeof(DrawInfo))]
    public class DrawSystem : AEntitySystem<float>
    {
        private readonly SpriteBatch _batch;
        public DrawSystem(SpriteBatch batch, World world)
            : base(world)
        {
            _batch = batch;
        }

        protected override void PreUpdate(float deltaTime)
        {
            _batch.Begin(SpriteSortMode.BackToFront);//, BlendState.AlphaBlend, SamplerState.PointClamp);
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            if (drawInfo.InView)
            {
                ref BoundingBox boundingBox = ref entity.Get<BoundingBox>();
                ref ScreenCoords screenCoords = ref entity.Get<ScreenCoords>();

                Rectangle dest = new Rectangle
                {
                    X = Mathf.RoundToInt(screenCoords.Position.X),
                    Y = Mathf.RoundToInt(screenCoords.Position.Y),
                    Width = Mathf.RoundToInt(drawInfo.Size.X * screenCoords.Scale.X),
                    Height = Mathf.RoundToInt(drawInfo.Size.Y * screenCoords.Scale.Y)
                };

                _batch.Draw(drawInfo.Texture, dest, drawInfo.SrcRect, drawInfo.Color, screenCoords.Rotation, drawInfo.Origin, SpriteEffects.None, drawInfo.LayerDepth);
            }
            /*
                        var tl = boundingBox.Position + new Vector2(3440/2,1440/2);
                        var tr = boundingBox.Position+new Vector2(boundingBox.Width,0) + new Vector2(3440/2,1440/2);
                        var bl = boundingBox.Position+new Vector2(0,boundingBox.Width) + new Vector2(3440/2,1440/2);
                        var br = boundingBox.Position+new Vector2(boundingBox.Width,boundingBox.Width) + new Vector2(3440/2,1440/2);

                        SpriteLine.DrawLine(_batch,tl,tr,Color.White,2f);
                        SpriteLine.DrawLine(_batch,tr,br,Color.White,2f);
                        SpriteLine.DrawLine(_batch,br,bl,Color.White,2f);
                        SpriteLine.DrawLine(_batch,bl,tl,Color.White,2f);
              */
        }


        protected override void PostUpdate(float deltaTime)
        {
            _batch.End();

        }
    }


    [With(typeof(ScreenCoords))]
    public class ResetDrawSystem : AEntitySystem<float>
    {
        //List<Entity> processed = new List<Entity>();
        public ResetDrawSystem(World world)
            : base(world)
        {

        }


        protected override void Update(float elaspedTime, in Entity entity)
        {
            entity.Remove<ScreenCoords>();
        }

    }
}