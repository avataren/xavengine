using DefaultEcs;
using DefaultEcs.System;
using DefaultEcs.Threading;
using Microsoft.Xna.Framework;
using XavEngine.Core.Components;
using XavEngine.Core.Physics2D;

namespace XavEngine.Core.Systems
{
    [With(typeof(Transform))]
    [With(typeof(TransformRoot))]
    [Without(typeof(TransformChild))]
    [With(typeof(DrawInfo))]
    [Without(typeof(tainicom.Aether.Physics2D.Dynamics.Body))]
    public sealed class TransformSystem : AEntitySystem<float>
    {
        static Matrix2D Identity = Matrix2D.Identity;
        public TransformSystem(World world,IParallelRunner runner)
            : base(world, runner)
        {
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            TransformWithChildren(entity, ref Identity);
        }

        private void TransformWithChildren(in Entity entity, ref Matrix2D parentTransform)
        {
            ref Transform localTransform = ref entity.Get<Transform>();
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            Matrix2D localSpace = localTransform.CreateMatrix();
            drawInfo.WorldTransform = localSpace * parentTransform;

            var children = entity.GetChildren();
            foreach (var c in children)
            {
                TransformWithChildren(c, ref drawInfo.WorldTransform);
            }
        }
    }

    [With(typeof(Transform))]
    [Without(typeof(TransformRoot))]
    [Without(typeof(TransformChild))]
    [With(typeof(DrawInfo))]
    [Without(typeof(tainicom.Aether.Physics2D.Dynamics.Body))]
    public sealed class TransformSystem_NoHierarchy : AEntitySystem<float>
    {
        public TransformSystem_NoHierarchy(World world,IParallelRunner runner)
            : base(world, runner)
        {
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref Transform localTransform = ref entity.Get<Transform>();
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            drawInfo.WorldTransform = localTransform.CreateMatrix();
        }
    }    

    [Without(typeof(TransformRoot))]
    [Without(typeof(TransformChild))]
    [With(typeof(Transform))]
    [With(typeof(DrawInfo))]
    [With(typeof(tainicom.Aether.Physics2D.Dynamics.Body))]
    public sealed class TransformSystem_Physics : AEntitySystem<float>
    {
        public TransformSystem_Physics(World world,IParallelRunner runner)
            : base(world, runner)
        {
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref Transform localTransform = ref entity.Get<Transform>();
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            ref tainicom.Aether.Physics2D.Dynamics.Body body = ref entity.Get<tainicom.Aether.Physics2D.Dynamics.Body>();
            localTransform.Position = new Vector2(  Physics.SI_TO_PIXELS(body.Position.X) , 
                                                    Physics.SI_TO_PIXELS(body.Position.Y) );
            localTransform.Rotation = body.Rotation;
            drawInfo.WorldTransform = localTransform.CreateMatrix();
        }
    }     

    [With(typeof(Transform))]
    [With(typeof(ScreenCoords))]
    public sealed class TransformToViewSystem : AEntitySystem<float>
    {
        readonly Camera camera;
        public TransformToViewSystem(World world, Camera camera, IParallelRunner runner)
            : base(world, runner)
        {
            this.camera = camera;
        }

        protected override void Update(float elaspedTime, in Entity entity)
        {
            ref DrawInfo drawInfo = ref entity.Get<DrawInfo>();
            if (!drawInfo.InView)
                return;

            ref ScreenCoords screen = ref entity.Get<ScreenCoords>();
            Matrix2D worldView = drawInfo.WorldTransform * camera.CameraTransform;
            screen.Position = worldView.Translation;
            screen.Rotation = -worldView.Rotation;
            screen.Scale = worldView.Scale / Mathf.Cos(screen.Rotation); //seems like divide by zero is "ok"
        }
    }
}