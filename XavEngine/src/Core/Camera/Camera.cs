using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XavEngine.Core;

namespace XavEngine.Core
{
    public class Camera
    {
        public Vector2 Position;
        public float Zoom { get; set; }
        public Rectangle Bounds { get; protected set; }
        public Rectangle VisibleArea { get; protected set; }
        public Matrix2D CameraTransform = Matrix2D.Identity;
        int previousMouseWheelValue = 0;
        int currentMouseWheelValue = 0;
        public BoundingBox AABB = new BoundingBox(Vector2.Zero, 0, 0);
        public Camera(Viewport viewport)
        {
            Bounds = viewport.Bounds;
            Zoom = 1f;
            Position = Vector2.Zero;
        }

        private void UpdateVisibleArea()
        {
           
            Matrix2D.Invert(ref CameraTransform, out Matrix2D inverseViewMatrix);

            var tl = Vector2.Transform(Vector2.Zero, inverseViewMatrix);
            var tr = Vector2.Transform(new Vector2(Bounds.X, 0), inverseViewMatrix);
            var bl = Vector2.Transform(new Vector2(0, Bounds.Y), inverseViewMatrix);
            var br = Vector2.Transform(new Vector2(Bounds.Width, Bounds.Height), inverseViewMatrix);

            var min = new Vector2(
                MathHelper.Min(tl.X, MathHelper.Min(tr.X, MathHelper.Min(bl.X, br.X))),
                MathHelper.Min(tl.Y, MathHelper.Min(tr.Y, MathHelper.Min(bl.Y, br.Y))));
            var max = new Vector2(
                MathHelper.Max(tl.X, MathHelper.Max(tr.X, MathHelper.Max(bl.X, br.X))),
                MathHelper.Max(tl.Y, MathHelper.Max(tr.Y, MathHelper.Max(bl.Y, br.Y))));
            VisibleArea = new Rectangle(Mathf.FastFloorToInt(min.X),
                                        Mathf.FastFloorToInt(min.Y),
                                        Mathf.FastFloorToInt(max.X - min.X),
                                        Mathf.FastFloorToInt(max.Y - min.Y));
            AABB.Bounds = VisibleArea;
        }

        public bool IsPointVisible (ref Vector2 point){
            if (point.X > VisibleArea.Right) return false;
            if (point.X < VisibleArea.Left) return false;
            if (point.Y < VisibleArea.Top) return false;
            if (point.Y > VisibleArea.Bottom) return false;
            return true;
        }
        private void UpdateMatrix()
        {
            CameraTransform = Matrix2D.CreateTranslation(new Vector2(-Position.X, -Position.Y)) *
                    Matrix2D.CreateScale(Zoom) *
                    Matrix2D.CreateTranslation(new Vector2(Bounds.Width * 0.5f, Bounds.Height * 0.5f));
            UpdateVisibleArea();
        }

        public void MoveCamera(Vector2 movePosition)
        {
            Vector2 newPosition = Position + movePosition;
            Position = newPosition;
        }

        public void Update(Viewport bounds, float deltaTime)
        {
            Bounds = bounds.Bounds;
            
                

            Vector2 cameraMovement = Vector2.Zero;
            int moveSpeed = 1500;

            if (!Console.Instance.Visible)
                cameraMovement = HandleInput(cameraMovement, moveSpeed);

            MoveCamera(cameraMovement * deltaTime);
            UpdateMatrix();
        }

        private Vector2 HandleInput(Vector2 cameraMovement, int moveSpeed)
        {
            if (Keyboard.GetState().IsKeyDown(KeyBindings.CameraMoveUp))
            {
                cameraMovement.Y = -moveSpeed;
            }

            if (Keyboard.GetState().IsKeyDown(KeyBindings.CameraMoveDown))
            {
                cameraMovement.Y = moveSpeed;
            }

            if (Keyboard.GetState().IsKeyDown(KeyBindings.CameraMoveLeft))
            {
                cameraMovement.X = -moveSpeed;
            }

            if (Keyboard.GetState().IsKeyDown(KeyBindings.CameraMoveRight))
            {
                cameraMovement.X = moveSpeed;
            }
            previousMouseWheelValue = currentMouseWheelValue;
            currentMouseWheelValue = Mouse.GetState().ScrollWheelValue;

            if (currentMouseWheelValue > previousMouseWheelValue)
            {
                Zoom *= 1.1f;
            }

            if (currentMouseWheelValue < previousMouseWheelValue)
            {
                Zoom *= 0.9f;
            }

            return cameraMovement;
        }
    }
}