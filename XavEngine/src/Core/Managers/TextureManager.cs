using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace XavEngine.Core.Managers
{
    public class TextureManager
    {
        readonly Dictionary<string, Texture2D> textures = null;
        static TextureManager Instance { get; set; }
        //static ContentManager Content { get; set; }
        GraphicsDevice device;
        public TextureManager(GraphicsDevice device)
        {
            if (Instance != null)
                throw new Exception ("Can only have one TextureManager");

            this.device = device;
            textures = new Dictionary<string, Texture2D>();
            Instance = this;

            Texture2D white = new Texture2D(device, 1,1);
            white.SetData(new Color[] {Color.White});
            Instance.textures["white"]=white;
        }

        public static Texture2D GetTexture(string name)
        {
            if (!Instance.textures.ContainsKey(name))
            {
                var tex = Load(name);
                if (tex == null)
                    throw new IndexOutOfRangeException($"Texture not found: {name}");
            }

            return Instance.textures[name];
        }

        public static Texture2D Load(string name)
        {
            if (Instance.textures.ContainsKey(name))
            {
                Instance.textures[name].Dispose();
                Instance.textures[name] = null;
            }
            using (var stream = TitleContainer.OpenStream(name))
            {
                Instance.textures[name] = Texture2D.FromStream(Instance.device, stream);
                
            }
            //var tex = Content.Load<Texture2D>(name);
            //Instance.textures[name] = tex;
            return Instance.textures[name];
        }
    }
}