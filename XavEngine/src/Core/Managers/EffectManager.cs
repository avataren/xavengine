using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace XavEngine.Core.Managers
{
    public class EffectManager
    {
        readonly Dictionary<string,Effect> effects = null;
        
        public EffectManager(){
            effects = new Dictionary<string,Effect>();
        }

        public Effect GetEffect (string name)
        {
            if (!effects.ContainsKey(name))
            {
                throw new NotImplementedException();
                //var eff = Load(name, Engine.Instance.Content);
                //if (eff == null)
                  //  throw new IndexOutOfRangeException($"Effect not found: {name}");
            }

            return effects[name];
        }
        
        public Effect Load(string name, ContentManager content)
        {
            if (effects.ContainsKey(name))
            {
                effects[name].Dispose();
                effects[name]=null;
            }

            var eff = content.Load<Effect>(name);            
            effects[name]=eff;
            return eff;
        }
    }
}