// using System.Collections.Generic;
// using Microsoft.Xna.Framework;
// using Microsoft.Xna.Framework.Graphics;

// namespace XavEngine.Core.Managers
// {
//     public class SpriteManager
//     {
//         SpriteBatch spriteBatch;
//         List<Sprite> BatchedSprites;
//         List<Sprite> EffectSprites;

//         public SpriteManager()
//         {
//             spriteBatch = new SpriteBatch(Engine.Instance.GraphicsDevice);
//             BatchedSprites = new List<Sprite>(10000);
//             EffectSprites = new List<Sprite>(10000);
//         }

//         public void Clear()
//         {
//             BatchedSprites.Clear();
//             EffectSprites.Clear();
//         }

//         public void DrawBatchedSprite(Sprite sprite)
//         {
//             BatchedSprites.Add(sprite);
//         }

//         public void DrawEffectSprite(Sprite sprite)
//         {
//             EffectSprites.Add(sprite);
//         }

//         public void Draw(GameTime gameTime)
//         {
//             spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, null);
//             foreach (Sprite sprite in BatchedSprites)
//             {
//                 if (sprite.IsInView(Engine.Instance.CurrentCamera))
//                     spriteBatch.Draw(sprite.texture, sprite.dstRectScaled, sprite.srcRect, sprite.color, sprite.compoundRotation, sprite.origin, SpriteEffects.None, sprite.LayerDepth);
//             }
//             spriteBatch.End();

//             EffectSprites.Sort((a, b) =>
//             {
//                 if (a.compoundRotation < b.compoundRotation) return -1;
//                 if (a.compoundRotation > b.compoundRotation) return 1;
//                 return 0;
//             });

//             foreach (Sprite sprite in EffectSprites)
//             {
//                 if (sprite.IsInView(Engine.Instance.CurrentCamera))
//                 {
//                     sprite.UpdateEffect(gameTime);
//                     spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, null, null, sprite.effect);
//                     spriteBatch.Draw(sprite.texture, sprite.dstRectScaled, sprite.srcRect, sprite.color, sprite.compoundRotation, sprite.origin, SpriteEffects.None, sprite.LayerDepth);
//                     if (sprite.visibleAABB)
//                         sprite.DrawAABB(spriteBatch, Engine.Instance.CurrentCamera.CameraTransform);
//                     spriteBatch.End();
//                 }
//             }
//         }
//     }
// }