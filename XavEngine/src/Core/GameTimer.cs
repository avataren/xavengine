using System.Diagnostics;

namespace XavEngine.Core
{

    public struct ElapsedTime
    {
        public float DeltaTime { get; set; }
        public float TotalTime { get; set; }
    }

    public class GameTimer
    {
        Stopwatch sw = null;
        long starTicks = 0;
        long lastTicks = 0;
        static GameTimer _instance = null;
        private ElapsedTime elapsed = new ElapsedTime();
        public static float TotalTime { get { return _instance.elapsed.TotalTime; } }
        public static float DeltaTime { get { return _instance.elapsed.DeltaTime; } }

        public GameTimer()
        {
            if (_instance != null)
                throw new System.Exception("GameTimer can only have one instance");

            _instance = this;
            Reset();
        }

        public void Reset()
        {
            sw = Stopwatch.StartNew();
            starTicks = lastTicks = sw.ElapsedTicks;
        }

        public ElapsedTime Update()
        {
            long elapsedTicks = sw.ElapsedTicks;
            elapsed.TotalTime = (elapsedTicks - starTicks) / (float)Stopwatch.Frequency;
            elapsed.DeltaTime = (elapsedTicks - lastTicks) / (float)Stopwatch.Frequency;
            lastTicks = elapsedTicks;
            return elapsed;
        }
    }
}