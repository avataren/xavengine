using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using XavEngine.Core.Managers;

namespace XavEngine.Core.Loaders
{

    public class MyRectangleConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var rectangle = (Rectangle)value;

            var x = rectangle.X;
            var y = rectangle.Y;
            var width = rectangle.Width;
            var height = rectangle.Height;
            var o = JObject.FromObject(new { x, y, width, height });

            o.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var o = JObject.Load(reader);

            var x = GetTokenValue(o, "x") ?? 0;
            var y = GetTokenValue(o, "y") ?? 0;
            var width = GetTokenValue(o, "w") ?? 0;
            var height = GetTokenValue(o, "h") ?? 0;

            return new Rectangle(x, y, width, height);
        }

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        private static int? GetTokenValue(JObject o, string tokenName)
        {
            JToken t;
            return o.TryGetValue(tokenName, StringComparison.InvariantCultureIgnoreCase, out t) ? (int)t : (int?)null;
        }
    }

    public class MyVector2Converter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var vector = (Vector2)value;

            var x = vector.X;
            var y = vector.Y;

            var o = JObject.FromObject(new { x, y });

            o.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var o = JObject.Load(reader);
            float x,y;

            float? rx = GetTokenValue(o, "w");
            if (rx != null)
            {
                x = (float)rx;
            } else x = GetTokenValue(o, "x") ?? 0;

            float? ry = GetTokenValue(o, "h");
            if (ry != null)
            {
                y = (float)ry;
            } else y = GetTokenValue(o, "y") ?? 0;

            return new Vector2(x, y);
        }

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        private static float? GetTokenValue(JObject o, string tokenName)
        {
            JToken t;
            return o.TryGetValue(tokenName, StringComparison.InvariantCultureIgnoreCase, out t) ? (float)t : (float?)null;
        }
    }

    public class SpriteSheetFrame
    {
        public string Filename { get; set; }
        [JsonConverter(typeof(MyRectangleConverter))]
        public Rectangle Frame { get; set; }
        public bool Rotated { get; set; }
        public bool Trimmed { get; set; }
        [JsonConverter(typeof(MyRectangleConverter))]
        public Rectangle SpriteSourceSize { get; set; }
        [JsonConverter(typeof(MyVector2Converter))]
        public Vector2 SourceSize { get; set; }
        [JsonConverter(typeof(MyVector2Converter))]
        public Vector2 Pivot { get; set; }
    }

    public class SpriteSheet
    {
        public Texture2D Texture {get; set;}
        public List<SpriteSheetFrame> Frames { get; set; }
    }


    public class SpriteSheetImporter
    {
        static public SpriteSheet Load(string fileName)
        {
            //Console.Log($"Importing JSON Spritesheet: {fileName}");
            using (var stream = TitleContainer.OpenStream(fileName))
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    var ssheet = JsonConvert.DeserializeObject<SpriteSheet>(sr.ReadToEnd());
                    ssheet.Texture = TextureManager.GetTexture(fileName.Replace(".json",".png"));
                    return ssheet;
                }
            }
        }
    }

}