using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace XavEngine.Core.Loaders
{
    public interface ITextureLoader
    {
        Texture2D Load(GraphicsDevice device, string fileName);
    }

    // public class TextureLoader : ITextureLoader
    // {
    //     public Texture2D Load(GraphicsDevice device, string fileName)
    //     {
    //         using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
    //         {
    //             Texture2D texture = Texture2D.FromStream(device, fileStream);
    //             return texture;
    //         }
    //     }
    // }

}