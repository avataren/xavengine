using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using DefaultEcs;
using DefaultEcs.Resource;
using DefaultEcs.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XavEngine.Core.Physics2D;
using XavEngine.Core.Systems;
using XavEngine.Core.Managers;

using Microsoft.Xna.Framework.Content;
using DefaultEcs.System;
using System.Threading;

namespace XavEngine.Core
{
    public class Core
    {
        public World World { get; set; }
        readonly IParallelRunner runner = new DefaultParallelRunner(Environment.ProcessorCount / 2);
        //TextureResourceManager textureResourceManager;
        readonly GraphicsDevice device = null;
        private readonly GameTimer gameTimer;
        // systems
        EmitterSystem emitterSystem;
        DrawSystem drawSystem;
        TransformSystem transformSystem;
        TransformSystem_NoHierarchy transformSystem_NoHierarchy;
        TransformToViewSystem transformToViewSystem;
        TransformSystem_Physics transformSystem_Physics;
        LifeCycleSystem lifeCycleSystem;
        CullingSystem cullingSystem;
        ParticleSystem particleSystem;

        SimplePhysicsSystem simplePhysicsSystem;
        AnimationSystem animationSystem;
        Console DebugConsole { get; set; }
        TextureManager textureManager = null;
        ISystem<float> transformSystems;
        public Physics Physics;
        ElapsedTime Elapsed { get; set; }
        public readonly Camera currentCam;

        public Core(GraphicsDevice device, ContentManager content)
        {
            try
            {
                var asm = Assembly.GetEntryAssembly();
                var exePath = new Uri(asm.CodeBase).LocalPath;
                var directory = Path.GetDirectoryName(exePath);
                Directory.SetCurrentDirectory(directory);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            currentCam = new Camera(device.Viewport);
            textureManager = new TextureManager(device);
            this.device = device;
            DebugConsole = new Console(device);
            DebugConsole.LoadContent();
            gameTimer = new GameTimer();
            World = new World();
            Physics = new Physics();
            InitializeSystems();
            Initialize();

        }

        protected void Initialize()
        {
            //textureResourceManager.Manage(World);
            World.Optimize(runner);
        }

        private void InitializeSystems()
        {
            // textureResourceManager = new TextureResourceManager(device, new TextureLoader());
            drawSystem = new DrawSystem(new SpriteBatch(device), World);
            transformSystem = new TransformSystem(World, runner);
            transformSystem_NoHierarchy = new TransformSystem_NoHierarchy(World, runner);
            transformToViewSystem = new TransformToViewSystem(World, currentCam, runner);
            cullingSystem = new CullingSystem(World, currentCam, runner);
            transformSystem_Physics = new TransformSystem_Physics(World, runner);
            animationSystem = new AnimationSystem(World, runner);
            lifeCycleSystem = new LifeCycleSystem(World, Physics);
            emitterSystem = new EmitterSystem(World);
            particleSystem = new ParticleSystem(World, runner);
            simplePhysicsSystem = new SimplePhysicsSystem(World, runner);
            transformSystems = new SequentialSystem<float>(
                simplePhysicsSystem,
                emitterSystem,
                particleSystem,
                lifeCycleSystem,
                transformSystem_Physics,
                transformSystem,
                transformSystem_NoHierarchy,
                cullingSystem,
                animationSystem,
                transformToViewSystem
            );
        }

        public void Update()
        {
            Elapsed = gameTimer.Update();
            currentCam.Update(device.Viewport, Elapsed.DeltaTime);
            //update systems
            transformSystems.Update(Elapsed.DeltaTime);
            DebugConsole.Update(Elapsed.DeltaTime);
        }

        public void Draw()
        {
            device.Clear(Color.Black);
            //update drawing systems
            drawSystem.Update(Elapsed.DeltaTime);
            DebugConsole.Draw(Elapsed);
            //device.Present();
        }

        public void Quit(){
            Physics.Quit();
        }
    }
}