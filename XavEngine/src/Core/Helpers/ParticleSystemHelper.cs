using DefaultEcs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XavEngine.Core.Components;

namespace XavEngine.Core.Helpers
{
    public class ParticleSystemHelper
    {
        public static Entity CreateParticleSystem(World world, Texture2D texture){
            var entity = world.CreateEntity();
            entity.Set(new Transform{Scale=new Vector2(1f)});
            // entity.Set(new DrawInfo{
            //     Color = Color.White,
            //     Origin = new Vector2(texture.Width / 2, texture.Height / 2),
            //     Center = new Vector2(texture.Width / 2, texture.Height / 2),
            //     SrcRect = null,
            //     Texture = texture,
            //     Size = new Vector2(texture.Width, texture.Height)                
            // });
            // entity.Set(new ScreenCoords());
            entity.Set(new Emitter{
                SpawnRate = 2000f,
                texture = texture
            });
            // entity.Set(new BoundingBox(Vector2.Zero, 0,0));

            return entity;
        }
    }
}