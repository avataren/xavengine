using DefaultEcs;
using DefaultEcs.Resource;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using tainicom.Aether.Physics2D.Dynamics;
using XavEngine.Core.Components;
using XavEngine.Core.Loaders;
using XavEngine.Core.Managers;
using XavEngine.Core.Systems;

namespace XavEngine.Core.Helpers
{
    public class SpriteHelper
    {
        public static Entity CreateAnimatedSprite(DefaultEcs.World world, SpriteSheet sheet, Entity? parent = null){
            Entity entity = world.CreateEntity();
            entity.Set(new Transform{Scale = Vector2.One});
            entity.Set(new ScreenCoords());
            var anim = new Animation(sheet);
            entity.Set(anim);
            var drawInfo = new DrawInfo
            {
                Color = Color.White,
                Size = anim.GetSize()
            };
            entity.Set(drawInfo);
            entity.Set(new BoundingBox(Vector2.Zero, 0,0));            
            return entity;

        }

        public static Entity CreateSprite(DefaultEcs.World world, string textureName, Entity? parent = null)
        {
            var texture = TextureManager.GetTexture(textureName);
            return CreateSprite(world,texture,parent);
        }

        public static Entity CreateSprite(DefaultEcs.World world, Texture2D texture, Entity? parent = null)
        {
            Entity entity = world.CreateEntity();
            entity.Set(new Transform{Scale = Vector2.One});
            entity.Set(new ScreenCoords());
            
            var drawInfo = new DrawInfo
            {
                Color = Color.White,
                Origin = new Vector2(texture.Width / 2, texture.Height / 2),
                Center = new Vector2(texture.Width / 2, texture.Height / 2),
                SrcRect = null,
                Texture = texture,
                Size = new Vector2(texture.Width, texture.Height)
            };
            entity.Set(drawInfo);
            entity.Set(new BoundingBox(Vector2.Zero, 0,0));

            if (parent != null)
            {
                entity.SetAsChildOf((Entity)parent);
                entity.Set(new TransformChild{ Parent = (Entity)parent});
                if (!((Entity)parent).Has<TransformChild>())
                {
                    ((Entity)parent).Set(new TransformRoot());
                }
            }
            return entity;
        }

        public static Entity CreateSprite(DefaultEcs.World world, Texture2D texture, int width, int height)
        {
            Entity entity = world.CreateEntity();
            entity.Set(new Transform{Scale = Vector2.One});
            entity.Set(new ScreenCoords());
            var drawInfo = new DrawInfo
            {
                Color = Color.White,
                Origin = new Vector2(texture.Width / 2, texture.Height / 2),
                Center = new Vector2(width / 2, height / 2),
                SrcRect = null,//new Rectangle(0,0,width,height),
                Texture = texture,
                Size = new Vector2(width, height)
            };
            entity.Set(drawInfo);
            entity.Set(new BoundingBox(Vector2.Zero, 0,0));
            return entity;
        }

        public static Entity CreateSpriteWithRigidBody(Core core, string textureName, BodyType bodyType)
        {
            Entity entity = core.World.CreateEntity();
            entity.Set(new Transform());
            entity.Set(new DrawInfo { Color = Color.White });
            var texture = TextureManager.GetTexture(textureName);
            var drawInfo = new DrawInfo
            {
                Color = Color.White,
                Origin = new Vector2(texture.Width / 2, texture.Height / 2),
                Center = new Vector2(texture.Width / 2, texture.Height / 2),
                SrcRect = null,
                Texture = texture,
                Size = new Vector2(texture.Width, texture.Height)
            };            
            entity.Set(drawInfo);
            entity.Set(new BoundingBox(Vector2.Zero, 0,0));
            entity.Set(core.Physics.World.CreateRectangle(drawInfo.Size.X, drawInfo.Size.Y,1f, Vector2.Zero, 0f, bodyType));
            return entity;
        }

    }
}