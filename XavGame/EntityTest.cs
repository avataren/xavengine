using System;
using System.Collections.Generic;
using System.Threading;
using DefaultEcs;
using DefaultEcs.Resource;
using DefaultEcs.System;
using DefaultEcs.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XavEngine;
using XavEngine.Core;
using XavEngine.Core.Components;
using XavEngine.Core.Helpers;
using XavEngine.Core.Loaders;
using XavEngine.Core.Managers;
using XavEngine.Core.Physics2D;
using XavEngine.Core.Systems;

namespace XavGame
{
    public class EntityTest : Game
    {
        Core core;
        readonly GraphicsDeviceManager Graphics;

        private int width;
        private int height;

        public EntityTest() : base()
        {
            width = 1920;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            height = 1080;//GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = width,
                PreferredBackBufferHeight = height,
                IsFullScreen = false,
                HardwareModeSwitch = false,
                SynchronizeWithVerticalRetrace = false,

            };
            IsFixedTimeStep = false;
            Graphics.ApplyChanges();
        }
        readonly string spriteFile1 = "Content/gfx/sprites/star.png";
        readonly string spriteFile2 = "Content/gfx/sprites/crate.png";

        Entity anim;
        Entity box;
        Entity particles;
        SpriteSheet sheet;
        protected override void Initialize()
        {
            width = Graphics.GraphicsDevice.PresentationParameters.Bounds.Width;
            height = Graphics.GraphicsDevice.PresentationParameters.Bounds.Height;
            core = new Core(GraphicsDevice, Content);

            sheet = SpriteSheetImporter.Load("Content/gfx/sprites/dance.json");

            Thread.Sleep(5);
            // SpriteSheet sheet = SpriteSheetImporter.Load("Content/gfx/sprites/dance.json");
            Random rnd = new Random(666);

            // var parent = SpriteHelper.CreateSprite(core,spriteFile);
            // parent.Set(new Transform{Position= new Vector2(0, -600), Scale=new Vector2(1f), Rotation=(float)Math.PI*0f});

            //  for (int i = 0; i < 500; i++)
            // {
            //     parent = SpriteHelper.CreateSprite(core,spriteFile,parent);
            //     parent.Set(new Transform{Position= new Vector2(256, 0), Scale=new Vector2(0.99f), Rotation=0.05f});
            //     ref DrawInfo di = ref parent.Get<DrawInfo>();
            //     di.LayerDepth = i/500f;
            // }
            /*
            for (int i = 0; i < 25000; i++)
            {
                //var crate = SpriteHelper.CreateSprite(core,spriteFile2);
                var testAnim = SpriteHelper.CreateAnimatedSprite(core, sheet);
                testAnim.Set(new Transform{
                                            Position= new Vector2(rnd.Next(-width*5,width*5), rnd.Next(-height*5,height*5)),
                                            Scale=new Vector2((float)(rnd.NextDouble()*3+1f)),
                                            Rotation=(float)(rnd.NextDouble()*Math.PI*2)
                                        });
                ref DrawInfo di = ref testAnim.Get<DrawInfo>();
                di.LayerDepth = i/25000f;

                ref Animation ani = ref testAnim.Get<Animation>();
                ani.RandomizeFrame();
            }
*/
            var world = core.World;
            particles = ParticleSystemHelper.CreateParticleSystem(world, TextureManager.GetTexture(spriteFile1));

            var floor = SpriteHelper.CreateSprite(world, TextureManager.GetTexture(spriteFile2), 1000, 20);

            var floor2 = SpriteHelper.CreateSprite(world, TextureManager.GetTexture(spriteFile2), 1000, 20);

            lock (core.Physics.lockObj)
            {
                ref DrawInfo fdrawInfo = ref floor.Get<DrawInfo>();
                fdrawInfo.LayerDepth = 0.1f;
                var fbod = core.Physics.World.CreateRectangle(Physics.PIXELS_TO_SI(fdrawInfo.Size.X), Physics.PIXELS_TO_SI(fdrawInfo.Size.Y), 1f, Vector2.Zero, 0.3f, tainicom.Aether.Physics2D.Dynamics.BodyType.Static);
                fbod.Position = new Vector2(0, Physics.PIXELS_TO_SI(200));
                floor.Set(fbod);

                fdrawInfo = ref floor2.Get<DrawInfo>();
                fdrawInfo.LayerDepth = 0.1f;
                fbod = core.Physics.World.CreateRectangle(Physics.PIXELS_TO_SI(fdrawInfo.Size.X), Physics.PIXELS_TO_SI(fdrawInfo.Size.Y), 1f, Vector2.Zero, -0.5f, tainicom.Aether.Physics2D.Dynamics.BodyType.Static);
                fbod.Position = new Vector2(Physics.PIXELS_TO_SI(400), Physics.PIXELS_TO_SI(650));
                floor2.Set(fbod);

            }

            box = SpriteHelper.CreateSprite(core.World, TextureManager.GetTexture(spriteFile2), 700, 100);
            
            lock (core.Physics.lockObj)
            
            {
                ref DrawInfo fdrawInfo = ref box.Get<DrawInfo>();
                fdrawInfo.LayerDepth = 0.05f;
                var fbox = core.Physics.World.CreateRectangle(Physics.PIXELS_TO_SI(fdrawInfo.Size.X), Physics.PIXELS_TO_SI(fdrawInfo.Size.Y), 1f, Vector2.Zero, 0.0f, tainicom.Aether.Physics2D.Dynamics.BodyType.Static);
                fbox.Position = new Vector2(0, Physics.PIXELS_TO_SI(100));
                box.Set(fbox);
            }
            base.Initialize();
        }

        bool bodiesSet = false;
        void AddBodies()
        {
            
            
            lock (core.Physics.lockObj)
            {
                bodiesSet = true;
                var rnd = new Random();
                for (int a = 0; a < 256; a++)
                {
                    //anim = SpriteHelper.CreateSprite(core, TextureManager.GetTexture(spriteFile2), 50, 50);
                    anim = SpriteHelper.CreateAnimatedSprite(core.World, sheet);
                    ref DrawInfo drawInfo = ref anim.Get<DrawInfo>();
                    var bod = core.Physics.World.CreateRectangle(   Physics.PIXELS_TO_SI((int)(drawInfo.Size.X*0.45)), 
                                                                    Physics.PIXELS_TO_SI((int)(drawInfo.Size.Y*0.75)), 
                                                                    50f,
                                                                    Vector2.Zero, 
                                                                    0f, 
                                                                    tainicom.Aether.Physics2D.Dynamics.BodyType.Dynamic);
                    //bod.Rotation = -0.5f;
                    bod.Position = new Vector2(rnd.Next(-600, 600), -rnd.Next(0, 12000)) * Physics.PIXELS_TO_SI(1);
                    anim.Set(bod);
                    anim.Set(new LifeTime { StartTime = GameTimer.TotalTime, MaxAge = 15 });
                    anim.Get<Animation>().RandomizeFrame();

                    //anim.Set(new KillBounds{KillRect = new Rectangle(-500,-500,1000,1000)});
                }
            }
        }
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Quit();

            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (!bodiesSet)
                    AddBodies();
            }

            if (Keyboard.GetState().IsKeyUp(Keys.Space))
                bodiesSet = false;

            var time = GameTimer.TotalTime;
            lock (core.Physics.lockObj)
            {
                ref tainicom.Aether.Physics2D.Dynamics.Body t = ref box.Get<tainicom.Aether.Physics2D.Dynamics.Body>();
                t.Rotation = time * 1f;
            }
            particles.Get<Transform>().Position = new Vector2(Mathf.Sin(time * .45f) * 400  ,  Mathf.Cos(time * .31f) * 400 );
            //core.currentCam.Position = new Vector2(Mathf.Sin(time * .145f) * width * 2.0f + Mathf.Cos(time * .019f) * width * 2.0f,
            //Mathf.Cos(time * .11f) * height * 4.0f);
            //core.currentCam.Zoom = .5f;


            core.Update();
        }
        protected override void Draw(GameTime gameTime)
        {
            core.Draw();
        }
        protected override void LoadContent()
        {
            PostLoadInitialize();
            base.LoadContent();
        }
        private void PostLoadInitialize()
        {

        }

        public void Quit()
        {
            core.Quit();
            Exit();
        }
    }
}
