using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using Newtonsoft.Json;

namespace XavPipeline
{

    public class MyRectangleConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var rectangle = (Rectangle)value;

            var x = rectangle.X;
            var y = rectangle.Y;
            var width = rectangle.Width;
            var height = rectangle.Height;

            var o = JObject.FromObject(new { x, y, width, height });

            o.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var o = JObject.Load(reader);

            var x = GetTokenValue(o, "x") ?? 0;
            var y = GetTokenValue(o, "y") ?? 0;
            var width = GetTokenValue(o, "w") ?? 0;
            var height = GetTokenValue(o, "h") ?? 0;

            return new Rectangle(x, y, width, height);
        }

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        private static int? GetTokenValue(JObject o, string tokenName)
        {
            JToken t;
            return o.TryGetValue(tokenName, StringComparison.InvariantCultureIgnoreCase, out t) ? (int)t : (int?)null;
        }
    }

    public class MyVector2Converter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var vector = (Vector2)value;

            var x = vector.X;
            var y = vector.Y;

            var o = JObject.FromObject(new { x, y });

            o.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var o = JObject.Load(reader);

            var x = GetTokenValue(o, "x") ?? 0;
            var y = GetTokenValue(o, "y") ?? 0;

            return new Vector2(x, y);
        }

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        private static int? GetTokenValue(JObject o, string tokenName)
        {
            JToken t;
            return o.TryGetValue(tokenName, StringComparison.InvariantCultureIgnoreCase, out t) ? (int)t : (int?)null;
        }
    }

    public class SpriteSheetFrame
    {

        public string Filename { get; set; }
        [JsonConverter(typeof(MyRectangleConverter))]
        public Rectangle Frame { get; set; }
        //"frame": {"x":0,"y":0,"w":104,"h":150},
        public bool Rotated { get; set; }
        //"trimmed": false,
        public bool Trimmed { get; set; }
        //"spriteSourceSize": {"x":0,"y":0,"w":104,"h":150},
        [JsonConverter(typeof(MyRectangleConverter))]
        public Rectangle SpriteSourceSize { get; set; }
        //"sourceSize": {"w":104,"h":150},
        [JsonConverter(typeof(MyVector2Converter))]
        public Vector2 SourceSize { get; set; }
        //"pivot": {"x":0.5,"y":1}
        [JsonConverter(typeof(MyVector2Converter))]
        public Vector2 Pivot { get; set; }
    }

    public class SpriteSheet
    {
        public List<SpriteSheetFrame> Frames { get; set; }
    }


    [ContentImporter(".json", DefaultProcessor = "SpriteSheetProcessor", DisplayName = "Sprite Sheet Importer")]
    public class SpriteSheetImporter : ContentImporter<SpriteSheet>
    {

        public override SpriteSheet Import(string fileName, ContentImporterContext context)
        {
            context.Logger.LogMessage("Importing JSON Spritesheet: {0}", fileName);


            var jsonString = File.ReadAllText(fileName);
            var ssheet = JsonConvert.DeserializeObject<SpriteSheet>(jsonString);
            return ssheet;
        }
    }
}