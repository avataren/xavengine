using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XavPipeline
{
    public class SpriteSheetProcessorResult
    {
        public SpriteSheet Sheet;
        public ContentBuildLogger Logger;

        public SpriteSheetProcessorResult(SpriteSheet ssheet, ContentBuildLogger logger)
        {
            Sheet = ssheet;
            Logger = logger;
        }
    }


    [ContentProcessor(DisplayName = "Tiled Map Processor")]
    public class SpriteSheetProcessor : ContentProcessor<SpriteSheet, SpriteSheetProcessorResult>
    {
        public override SpriteSheetProcessorResult Process(SpriteSheet ssheet, ContentProcessorContext context)
        {
            return new SpriteSheetProcessorResult(ssheet, context.Logger);
        }
    }
    
}