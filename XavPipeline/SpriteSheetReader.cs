using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace XavPipeline
{
    public class SpriteSheetReader : ContentTypeReader<SpriteSheet>
    {
        protected override SpriteSheet Read(ContentReader input, SpriteSheet existingInstance)
        {
            var frameCount = input.ReadInt32();
            var frames = new List<SpriteSheetFrame>();

            for (var i = 0; i < frameCount; i++)
            {
                var fileName = input.ReadString();
                var frameX = input.ReadInt32();
                var frameY = input.ReadInt32();
                var frameW = input.ReadInt32();
                var frameH = input.ReadInt32();
                var rotated = input.ReadBoolean();
                var trimmed = input.ReadBoolean();
                var spriteX = input.ReadInt32();
                var spriteY = input.ReadInt32();
                var spriteW = input.ReadInt32();
                var spriteH = input.ReadInt32();
                var sourceSize = input.ReadVector2();
                var pivot = input.ReadVector2();

                frames.Add(new SpriteSheetFrame
                {
                    Filename = fileName,
                    Frame = new Microsoft.Xna.Framework.Rectangle(frameX, frameY, frameW, frameH),
                    Rotated = rotated,
                    Trimmed = trimmed,
                    SpriteSourceSize = new Microsoft.Xna.Framework.Rectangle(spriteX, spriteY, spriteW, spriteH),
                    SourceSize = sourceSize,
                    Pivot = pivot
                });
            }

            //var json = input.ReadString();
            //var fontFile = JsonConvert.DeserializeObject<FontFile>(json);
            //var texture = input.ContentManager.Load<Texture2D>(assets.First());
            return new SpriteSheet { Frames = frames };
        }
    }
}