using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace XavPipeline
{

    [ContentTypeWriter]
    public class SpriteSheetWriter : ContentTypeWriter<SpriteSheet>
    {
        protected override void Write (ContentWriter output, SpriteSheet sheet)
        {
            output.Write(sheet.Frames.Count);
            foreach (var frame in sheet.Frames)
            {
                output.Write(frame.Filename);
                output.Write(frame.Frame.X);
                output.Write(frame.Frame.Y);
                output.Write(frame.Frame.Width);
                output.Write(frame.Frame.Height);
                output.Write(frame.Rotated);
                output.Write(frame.Trimmed);
                output.Write(frame.SpriteSourceSize.X);
                output.Write(frame.SpriteSourceSize.Y);
                output.Write(frame.SpriteSourceSize.Width);
                output.Write(frame.SpriteSourceSize.Height);
                output.Write(frame.SourceSize);
                output.Write(frame.Pivot);
            }
            /*
            output.Write(value.TextureAssets.Count);

            foreach (var textureAsset in value.TextureAssets)
                output.Write(textureAsset);

            output.Write(value.Json);*/
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(SpriteSheet).AssemblyQualifiedName;
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(SpriteSheetReader).AssemblyQualifiedName;
            //return "XavPipeline.SpriteSheetReader, XavPipeline";
            //return "MonoGame.Extended.BitmapFonts.BitmapFontReader, MonoGame.Extended";
        }
    }
}