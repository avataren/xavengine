using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using XavEngine;
using XavGame;

namespace AndroidApp
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : EntityTest
    {


        public Game1() : base()
        {
            TouchPanel.EnabledGestures = GestureType.Pinch;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
           /* width = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            height = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.IsFullScreen = true;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();
            */
            // TODO: Add your initialization logic here
            base.Initialize();
            //DebugConsole.Visible = true;

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            HandleTouchInput();
            base.Update(gameTime);
        }
        bool _pinching = false;
        float _pinchInitialDistance;

        private void HandleTouchInput()
        {
            while (TouchPanel.IsGestureAvailable)
            {
                GestureSample gesture = TouchPanel.ReadGesture();
                if (gesture.GestureType == GestureType.Pinch)
                {
                    // current positions
                    Vector2 a = gesture.Position;
                    Vector2 b = gesture.Position2;
                    float dist = Vector2.Distance(a, b);

                    // prior positions
                    Vector2 aOld = gesture.Position - gesture.Delta;
                    Vector2 bOld = gesture.Position2 - gesture.Delta2;
                    float distOld = Vector2.Distance(aOld, bOld);

                    if (!_pinching)
                    {
                        // start of pinch, record original distance
                        _pinching = true;
                        _pinchInitialDistance = distOld;
                    }

                    // work out zoom amount based on pinch distance...
                    float scale = (distOld - dist) * -0.001f;
                    //ZoomBy(scale);
                }
                else if (gesture.GestureType == GestureType.PinchComplete)
                {
                    // end of pinch
                    _pinching = false;
                }
            }
        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
