﻿using System;
using XavGame;
namespace DesktopDX
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new EntityTest())
                game.Run();
        }
    }
}
