﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using XavEditor.Controls;
using Microsoft.Xna.Framework;
using XavEngine;

namespace XavEditor
{
    public partial class UserControl1 : MonoGameControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        protected override void Initialize()
        {
            base.Initialize();

            Editor.engine.LoadContent();
            PostLoadInitialize();

        }

        private void PostLoadInitialize()
        {
            var sprite = new Sprite("TestSprite", Engine.TextureManager.GetTexture("gfx/sprites/pentagram"))
            {
                Transform = new Transform2
                {
                    //Scale = new Vector2(0.9f),
                },
                angularVelocity = 1f,
                LayerDepth = 1
            };

int ccount = 16;
            GameObject c1 = sprite.Clone();
            c1.Transform.Position = new Vector2(0, 256 + 128);
            c1.LayerDepth = 0.99999f;
            GameObject parnt1 = c1;
            for (int i = 1; i <= ccount; i++)
            {
                var chld = parnt1.Clone();
                chld.Transform.Position = new Vector2(0, 256 + 128);
                chld.Transform.Scale = new Vector2(0.9f);
                chld.LayerDepth = 1 - i / (float)ccount;
                parnt1.AddChild(chld);
                parnt1 = chld;
            }

            GameObject c2 = sprite.Clone();
            c2.Transform.Position = new Vector2(0, -256 - 128);
            c2.LayerDepth = 0.99999f;
            GameObject parnt2 = c2;
            for (int i = 1; i <= ccount; i++)
            {
                var chld = parnt2.Clone();
                chld.Transform.Position = new Vector2(0, -256 - 128);
                chld.Transform.Scale = new Vector2(0.9f);
                chld.LayerDepth = 1 - i / (float)ccount;
                parnt2.AddChild(chld);
                parnt2 = chld;
            }

            GameObject c3 = sprite.Clone();
            c3.Transform.Position = new Vector2(-256 - 128,0);
            c3.LayerDepth = 0.99999f;
            GameObject parnt3 = c3;
            for (int i = 1; i <= ccount; i++)
            {
                var chld = parnt3.Clone();
                chld.Transform.Position = new Vector2(-256 - 128, 0);
                chld.Transform.Scale = new Vector2(0.9f);
                chld.LayerDepth = 1 - i / (float)ccount;
                parnt3.AddChild(chld);
                parnt3 = chld;
            }

            GameObject c4 = sprite.Clone();
            c4.Transform.Position = new Vector2(256 + 128,0);
            c4.LayerDepth = 0.99999f;
            GameObject parnt4 = c4;
            for (int i = 1; i <= ccount; i++)
            {
                var chld = parnt4.Clone();
                chld.Transform.Position = new Vector2(256 + 128, 0);
                chld.Transform.Scale = new Vector2(0.9f);
                chld.LayerDepth = 1 - i / (float)ccount;
                parnt4.AddChild(chld);
                parnt4 = chld;
            }

            sprite.AddChild(c1);
            sprite.AddChild(c2);
            sprite.AddChild(c3);
            sprite.AddChild(c4);

            //List<GameObject> sprites = new List<GameObject>();
            
            for (int y=0;y<7;y++)
            for (int x=0;x<7;x++)
            {
                //if (x==3 && y==3) continue;
                var spr = sprite.Clone();
                
                spr.Transform.Position = spr.Transform.Position+new Vector2((x-3)*2000,(y-3)*2000);
                Engine.Instance.scene.AddChild(spr);
                //sprites.Add(spr);
            }

            //foreach(var s in sprites)
                //engine.scene.AddChild(s);

           // engine.scene.AddChild(sprite);
        }
    }
}
